
# Simple MLM Web

A Simple MLM System to show members in a hierarchical tree. show members' level and bonus based on their downline



## Installation

Project Requirements

```
  Flutter 3.0.5+
```

Run project

```bash
  flutter run
```

Build web app

```bash
  flutter build web
```

Install PM2 to serve the web app

```bash
  npm install -g pm2
```
    
Serve the web app using pm2

```bash
  pm2 serve build/web/ --name=simple-mlm-web --port=8080
```

you can access the web app on http://127.0.0.1:8080

