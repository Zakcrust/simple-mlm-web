import 'package:dio/dio.dart';
import 'package:simple_mlm/utils/global_dio.dart';

class MembersRepositories {
  Future<Response> getMember(Map<String, dynamic> data, {String token = ''}) {
    return Future.delayed(const Duration(milliseconds: 10), () async {
      try {
        final response = await DIOGlobal.dio.get('members');
        return response;
      } on DioError catch (_, e) {
        print(e);
        print(_.response);
        return _.response!;
      }
    });
  }

  Future<Response> addMember(Map<String, dynamic> data, {String token = ''}) {
    return Future.delayed(const Duration(milliseconds: 10), () async {
      try {
        var body = {};
        body['name'] = data['name'];
        if(data['parent'] == 'admin')
        {
          body['firstMember'] = 'true';
        }
        else {
          body['parent'] = data['parent'];
        }
        final response = DIOGlobal.dio.post('members', data: body);
        return response;
      } on DioError catch (_, e) {
        return _.response!;
      }
    });
  }

  Future<Response> migrateMember(Map<String, dynamic> data,
      {String token = ''}) {
    return Future.delayed(const Duration(milliseconds: 10), () async {
      try {
        var body = {};
         if(data['parent'] == 'admin')
        {
          body['firstMember'] = 'true';
        }
        else {
          body['parent'] = data['parent'];
        }
        final response = DIOGlobal.dio.put('members/change-parent/${data['id']}', data: body);
        return response;
      } on DioError catch (_, e) {
        return _.response!;
      }
    });
  }

  Future<Response> getMemberBonus(Map<String, dynamic> data,
      {String token = ''}) {
    return Future.delayed(const Duration(milliseconds: 10), () async {
      try {
        final response = DIOGlobal.dio.get('members/bonus/${data['id']}?level=${data['level']}');
        return response;
      } on DioError catch (_, e) {
        return _.response!;
      }
    });
  }

  Future<Response> getParentsToMigrate(Map<String, dynamic> data,
      {String token = ''}) {
    return Future.delayed(const Duration(milliseconds: 10), () async {
      try {
        final response = DIOGlobal.dio.get('members/parents-to-migrate/${data['id']}');
        return response;
      } on DioError catch (_, e) {
        return _.response!;
      }
    });
  }
}
