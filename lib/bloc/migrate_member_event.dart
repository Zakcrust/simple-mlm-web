part of 'migrate_member_bloc.dart';

@immutable
abstract class MigrateMemberEvent {}

class MigrateMember extends MigrateMemberEvent {
  final String id;
  final String parent;
  MigrateMember(this.id, this.parent);
}