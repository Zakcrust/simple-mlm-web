part of 'members_bloc.dart';

@immutable
abstract class MembersState {}

class MembersInitial extends MembersState {}

class GetMemberLoading extends MembersState {}

class GetMembersSuccess extends MembersState {
  final MemberReponse memberReponse;
  GetMembersSuccess(this.memberReponse);
}

class GetMembersFailed extends MembersState {
  final String message;
  GetMembersFailed(this.message);
}