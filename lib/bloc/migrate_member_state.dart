part of 'migrate_member_bloc.dart';

@immutable
abstract class MigrateMemberState {}

class MigrateMemberInitial extends MigrateMemberState {}

class MigrateMemberLoading extends MigrateMemberState {}

class MigrateMemberSuccess extends MigrateMemberState {
  final MemberMigrationResponse memberMigrationResponse;
  MigrateMemberSuccess(this.memberMigrationResponse);
}

class MigrateMemberFailed extends MigrateMemberState {
  final String message;
  MigrateMemberFailed(this.message);
}