import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:simple_mlm/models/member_migration_response.dart';
import 'package:simple_mlm/repositories/members_repositories.dart';

part 'migrate_member_event.dart';
part 'migrate_member_state.dart';

class MigrateMemberBloc extends Bloc<MigrateMemberEvent, MigrateMemberState> {
  MembersRepositories memberRepositories = MembersRepositories();
  MigrateMemberBloc() : super(MigrateMemberInitial()) {
    on<MigrateMemberEvent>((event, emit) async {
      try {
        if (event is MigrateMember) {
          emit(MigrateMemberLoading());
          final response = await memberRepositories
              .migrateMember({'id': event.id, 'parent': event.parent});
          if (response.statusCode == 200) {
            emit(MigrateMemberSuccess(
                MemberMigrationResponse.fromJson(response.data)));
          } else {
            emit(MigrateMemberFailed(response.data['message']));
          }
        }
      } catch (e) {
        emit(MigrateMemberFailed('Unhandled exception\n$e'));
      }
    });
  }
}
