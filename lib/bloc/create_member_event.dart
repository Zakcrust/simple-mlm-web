part of 'create_member_bloc.dart';

@immutable
abstract class CreateMemberEvent {}

class CreateMember extends CreateMemberEvent{
  final String name;
  final String parent;
  CreateMember(this.name, this.parent);
}