import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:simple_mlm/models/member_create_response.dart';
import 'package:simple_mlm/repositories/members_repositories.dart';

part 'create_member_event.dart';
part 'create_member_state.dart';

class CreateMemberBloc extends Bloc<CreateMemberEvent, CreateMemberState> {
  MembersRepositories memberRepositories = MembersRepositories();
  CreateMemberBloc() : super(CreateMemberInitial()) {
    on<CreateMemberEvent>((event, emit) async {
      try {
        if (event is CreateMember) {
          emit(CreateMemberLoading());
          final response = await memberRepositories
              .addMember({'name': event.name, 'parent': event.parent});

          if (response.statusCode == 201) {
            emit(CreateMemberSuccess(
                MemberCreateResponse.fromJson(response.data)));
          } else {
            emit(CreateMemberFailed(response.data['message']));
          }
        }
      } catch (e) {
        emit(CreateMemberFailed('Unhandlred exception\n$e'));
      }
    });
  }
}
