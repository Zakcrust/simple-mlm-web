part of 'create_member_bloc.dart';

@immutable
abstract class CreateMemberState {}

class CreateMemberInitial extends CreateMemberState {}

class CreateMemberLoading extends CreateMemberState {}

class CreateMemberSuccess extends CreateMemberState {
  final MemberCreateResponse memberCreateResponse;
  CreateMemberSuccess(this.memberCreateResponse);
}

class CreateMemberFailed extends CreateMemberState {
  final String message;
  CreateMemberFailed(this.message);
}