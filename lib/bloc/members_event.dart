part of 'members_bloc.dart';

@immutable
abstract class MembersEvent {}

class GetMembers extends MembersEvent {}
