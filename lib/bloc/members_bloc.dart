import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:simple_mlm/models/member_response.dart';
import 'package:simple_mlm/repositories/members_repositories.dart';

part 'members_event.dart';
part 'members_state.dart';

class MembersBloc extends Bloc<MembersEvent, MembersState> {
  MembersRepositories membersRepositories = MembersRepositories();
  MembersBloc() : super(MembersInitial()) {
    on<MembersEvent>((event, emit) async {
      try {
        if (event is GetMembers) {
          emit(GetMemberLoading());
          final result = await membersRepositories.getMember({});
          if (result.statusCode == 200) {
            emit(GetMembersSuccess(MemberReponse.fromJson(result.data)));
          } else {
            emit(GetMembersFailed(result.data['message']));
          }
        }
      } catch (e) {
        emit(GetMembersFailed('Unhandled exception\n$e'));
      }
    });
  }
}
