// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:simple_mlm/bloc/create_member_bloc.dart';
import 'package:simple_mlm/bloc/members_bloc.dart';
import 'package:simple_mlm/bloc/migrate_member_bloc.dart';
import 'package:simple_mlm/models/members.dart';
import 'package:simple_mlm/models/migrate_parents_response.dart';
import 'package:simple_mlm/repositories/members_repositories.dart';
import 'package:simple_mlm/size_config.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_mlm/utils/general_tool.dart';
import 'models/member_response.dart';
import 'package:graphview/GraphView.dart';

void main() {
  runApp(const Home());
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Simple MLM Admin Panel',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Simple MLM Admin Panel'),
        ),
        body: MultiBlocProvider(
          providers: [
            BlocProvider<MembersBloc>(
              create: (context) => MembersBloc(),
            ),
            BlocProvider<CreateMemberBloc>(
              create: (context) => CreateMemberBloc(),
            ),
            BlocProvider<MigrateMemberBloc>(
              create: (context) => MigrateMemberBloc(),
            ),
          ],
          child: const Content(),
        ),
      ),
    );
  }
}

class Content extends StatefulWidget {
  const Content({Key? key}) : super(key: key);

  @override
  State<Content> createState() => _ContentState();
}

class _ContentState extends State<Content> {
  List<Member> members = [
    Member(sId: '12', name: 'Test', level: 1, bonus: 2.5, parent: "admin"),
    Member(sId: '123', name: 'Test 2', level: 2, bonus: 2.5, parent: "12"),
    Member(sId: '123', name: 'Test 2', level: 3, bonus: 2.5, parent: "12")
  ];
  Member? memberDropDownValue;
  Member? memberBaruParentDropDownValue;
  Member? migratiMemberParentDropDownValue;
  Member? migrateMemberDropDownValue;
  List<int> levels = [];
  int? levelDropDownValue;
  String bonusResult = 'Result';
  String memberBaruName = '';
  List<Member> memberBaruParents = [];
  List<Member> migrasiMemberParents = [];

  Graph graph = Graph()..isTree = true;
  BuchheimWalkerConfiguration builder = BuchheimWalkerConfiguration();
  List<MemberNodes> nodes = [
    MemberNodes(
        Member(sId: 'admin', name: 'Admin', level: 3, bonus: 2.5, parent: ""),
        Node.Id(1))
  ];

  void initData() {
    graph = Graph()..isTree = true;
    memberBaruName = '';
    memberDropDownValue = null;
    memberBaruParentDropDownValue = null;
    migratiMemberParentDropDownValue = null;
    migrateMemberDropDownValue = null;
    levels = [];
    levelDropDownValue = null;
    memberBaruParents = [
      Member(sId: 'admin', name: 'Admin', level: 3, bonus: 2.5, parent: "")
    ];
    memberBaruParents = [...memberBaruParents, ...members];
    migrasiMemberParents = [];
    nodes = [
    MemberNodes(
        Member(sId: 'admin', name: 'Admin', level: 3, bonus: 2.5, parent: ""),
        Node.Id(1))
  ];
    for (var i = 0; i < members.length; i++) {
      nodes.add(MemberNodes(members[i], Node.Id(i + 2)));
    }
    for (var i = 0; i < nodes.length; i++) {
      if (nodes[i].member.sId == 'admin') continue;
      var parentFound = nodes.firstWhere(
          (element) => element.member.sId == nodes[i].member.parent);
      graph.addEdge(parentFound.node, nodes[i].node);
    }
    builder
      ..siblingSeparation = (25)
      ..levelSeparation = (25)
      ..subtreeSeparation = (35)
      ..orientation = (BuchheimWalkerConfiguration.ORIENTATION_TOP_BOTTOM);
    setState(() {});
  }

  @override
  void initState() {
    context.read<MembersBloc>().add(GetMembers());
    super.initState();
  }

  void getPossibleParent(context) async {
    try {
      migratiMemberParentDropDownValue = null;
      migrasiMemberParents = [];
      MembersRepositories membersRepositories = MembersRepositories();
      showDialog(
          context: context,
          builder: (context) => GeneralTool().loaderDialog('Loading...'));
      final response = await membersRepositories.getParentsToMigrate(
          {'id': migrateMemberDropDownValue?.sId});
      Navigator.pop(context);
      print(response.data);
      if (response.statusCode == 200) {
        MigrateParentResponse migrateParents = MigrateParentResponse.fromJson(response.data);
        if(migrateMemberDropDownValue?.parent != 'admin')
        {
          migrasiMemberParents = [Member(sId: 'admin', name: 'Admin', level: 3, bonus: 2.5, parent: "")];
        }
        setState(() {
          migrasiMemberParents = [...migrasiMemberParents, ...migrateParents.data!];
        });
      } else {
        GeneralTool().customDialog(
            context, "Error", 'Member not found', '', DialogType.noHeader);
      }
    } on DioError catch (_, e) {
      Navigator.pop(context);
      GeneralTool().customDialog(context, "Error", _.response?.data['message'],
          '', DialogType.noHeader);
    }
  }

  void hitungBonus(context) async {
    try {
      MembersRepositories membersRepositories = MembersRepositories();
      showDialog(
          context: context,
          builder: (context) => GeneralTool().loaderDialog('Loading...'));
      final response = await membersRepositories.getMemberBonus(
          {'id': memberDropDownValue?.sId, 'level': levelDropDownValue});
      Navigator.pop(context);
      if (response.statusCode == 200) {
        setState(() {
          bonusResult = "Bonus : \$${response.data['data']['bonus']}";
        });
      } else {
        GeneralTool().customDialog(
            context, "Error", 'Member not found', '', DialogType.noHeader);
      }
    } on DioError catch (_, e) {
      Navigator.pop(context);
      GeneralTool().customDialog(context, "Error", _.response?.data['message'],
          '', DialogType.noHeader);
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SingleChildScrollView(
      child: SizedBox(
        height: SizeConfig.screenHeight! * 2,
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.screenWidth! * 0.05,
              vertical: SizeConfig.screenHeight! * 0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              BlocListener<MembersBloc, MembersState>(
                listener: (context, state) {
                  if (state is GetMemberLoading) ;
                  {
                    // showDialog(
                    //     context: context,
                    //     builder: (context) =>
                    //         GeneralTool().loaderDialog('Getting Members...'));
                  }
                  if (state is GetMembersSuccess) {
                    //Navigator.pop(context);
                    setState(() {
                      members = [];
                      members = state.memberReponse.data!;
                    });
                    initData();
                  }
                  if (state is GetMembersFailed) {
                    //Navigator.pop(context);
                    GeneralTool().customDialog(context, 'Failed', state.message,
                        '', DialogType.noHeader);
                  }
                },
                child: Container(),
              ),
              BlocListener<CreateMemberBloc, CreateMemberState>(
                listener: (context, state) {
                  if (state is CreateMemberLoading) {
                    showDialog(
                        context: context,
                        builder: (context) =>
                            GeneralTool().loaderDialog('Loading...'));
                  }
                  if (state is CreateMemberSuccess) {
                    Navigator.pop(context);
                    GeneralTool().customDialog(
                        context,
                        'Success',
                        state.memberCreateResponse.message!,
                        '',
                        DialogType.noHeader);
                    context.read<MembersBloc>().add(GetMembers());
                  }
                  if (state is CreateMemberFailed) {
                    Navigator.pop(context);
                    GeneralTool().customDialog(context, 'Failed', state.message,
                        '', DialogType.noHeader);
                  }
                },
                child: Container(),
              ),
              BlocListener<MigrateMemberBloc, MigrateMemberState>(
                listener: (context, state) {
                  if (state is MigrateMemberLoading) {
                    showDialog(
                        context: context,
                        builder: (context) =>
                            GeneralTool().loaderDialog('Loading...'));
                  }
                  if (state is MigrateMemberSuccess) {
                    Navigator.pop(context);
                    GeneralTool().customDialog(
                        context,
                        'Success',
                        state.memberMigrationResponse.message!,
                        '',
                        DialogType.noHeader);
                    context.read<MembersBloc>().add(GetMembers());
                  }
                  if (state is MigrateMemberFailed) {
                    Navigator.pop(context);
                    GeneralTool().customDialog(context, 'Failed', state.message,
                        '', DialogType.noHeader);
                  }
                },
                child: Container(),
              ),
              hitungBonusForm(context),
              SizedBox(
                height: SizeConfig.screenHeight! * 0.02,
              ),
              registrasiMemberForm(context),
              SizedBox(
                height: SizeConfig.screenHeight! * 0.02,
              ),
              migrasiMemberForm(context),
              SizedBox(
                height: SizeConfig.screenHeight! * 0.02,
              ),
              BlocBuilder<MembersBloc, MembersState>(
                builder: (context, state) {
                  if (state is GetMemberLoading) {
                    return const CircularProgressIndicator();
                  }
                  if (state is GetMembersSuccess) {
                    return Expanded(
                        child: Container(
                            margin: const EdgeInsets.all(8.0),
                            child: treeGraph(context)));
                  }
                  return Container();
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget hitungBonusForm(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.screenHeight! * 0.02),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('Perhitungan Bonus',
              style: TextStyle(fontWeight: FontWeight.bold)),
          Row(
            children: [
              SizedBox(
                // height: SizeConfig.screenHeight! * 0.05,
                width: SizeConfig.screenWidth! * 0.2,
                child: DropdownButton<Member>(
                  isExpanded: true,
                  value: memberDropDownValue,
                  hint: const Text('Select Member'),
                  // icon: const Icon(Icons.arrow_downward),
                  elevation: 16,
                  style: const TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (Member? value) {
                    // This is called when the user selects an item.
                    levels = [];
                    for (var i = value!.level; i! < value.level! + 2; i++) {
                      levels.add(i + 1);
                    }
                    levelDropDownValue = levels.first;
                    setState(() {
                      memberDropDownValue = value;
                    });
                  },
                  items: members.map<DropdownMenuItem<Member>>((Member value) {
                    return DropdownMenuItem<Member>(
                      value: value,
                      child: Text(value.name!),
                    );
                  }).toList(),
                ),
              ),
              SizedBox(
                width: SizeConfig.screenWidth! * 0.04,
              ),
              SizedBox(
                // height: SizeConfig.screenHeight! * 0.05,
                width: SizeConfig.screenWidth! * 0.2,
                child: DropdownButton<int>(
                  isExpanded: true,
                  value: levelDropDownValue,
                  hint: const Text('Select Level'),
                  // icon: const Icon(Icons.arrow_downward),
                  elevation: 16,
                  style: const TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (int? value) {
                    // This is called when the user selects an item.
                    setState(() {
                      levelDropDownValue = value!;
                    });
                  },
                  items: levels.map<DropdownMenuItem<int>>((int value) {
                    return DropdownMenuItem<int>(
                      value: value,
                      child: Text('Level ${value.toString()}'),
                    );
                  }).toList(),
                ),
              ),
              SizedBox(
                width: SizeConfig.screenWidth! * 0.04,
              ),
              GestureDetector(
                onTap: () {
                  if (memberDropDownValue == null) {
                    GeneralTool().customDialog(
                        context,
                        "Warning",
                        "Select member to count their bonus",
                        '',
                        DialogType.noHeader);
                    return;
                  }
                  if (levelDropDownValue == null) {
                    GeneralTool().customDialog(
                        context,
                        "Warning",
                        "Choose a level",
                        '',
                        DialogType.noHeader);
                    return;
                  }
                  hitungBonus(context);
                },
                child: Container(
                  height: SizeConfig.screenHeight! * 0.05,
                  width: SizeConfig.screenWidth! * 0.08,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(10.0)),
                  child: const Center(
                      child: Text(
                    'Submit',
                    style: TextStyle(color: Colors.white),
                  )),
                ),
              ),
              SizedBox(
                width: SizeConfig.screenWidth! * 0.02,
              ),
              Container(
                height: SizeConfig.screenHeight! * 0.05,
                width: SizeConfig.screenWidth! * 0.1,
                decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 142, 138, 138),
                    borderRadius: BorderRadius.circular(10.0)),
                child: Center(
                    child: Text(
                  bonusResult,
                  style: const TextStyle(color: Colors.white),
                )),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget registrasiMemberForm(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.screenHeight! * 0.02),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('Registrasi Member Baru',
              style: TextStyle(fontWeight: FontWeight.bold)),
          Row(
            children: [
              SizedBox(
                // height: SizeConfig.screenHeight! * 0.05,
                width: SizeConfig.screenWidth! * 0.2,
                child: TextFormField(
                  onChanged: (value) => memberBaruName = value,
                  validator: (value) => value!.length < 5
                      ? "Name must be longer than 5 characters"
                      : null,
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: 'Inisial Member Baru',
                  ),
                ),
              ),
              SizedBox(
                width: SizeConfig.screenWidth! * 0.04,
              ),
              SizedBox(
                // height: SizeConfig.screenHeight! * 0.05,
                width: SizeConfig.screenWidth! * 0.2,
                child: DropdownButton<Member>(
                  isExpanded: true,
                  value: memberBaruParentDropDownValue,
                  hint: const Text('Select Parent'),
                  // icon: const Icon(Icons.arrow_downward),
                  elevation: 16,
                  style: const TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (Member? value) {
                    // This is called when the user selects an item.
                    setState(() {
                      memberBaruParentDropDownValue = value!;
                    });
                  },
                  items: memberBaruParents
                      .map<DropdownMenuItem<Member>>((Member value) {
                    return DropdownMenuItem<Member>(
                      value: value,
                      child: Text(value.name!),
                    );
                  }).toList(),
                ),
              ),
              SizedBox(
                width: SizeConfig.screenWidth! * 0.04,
              ),
              GestureDetector(
                onTap: () {
                  if (memberBaruName.length < 5) {
                    GeneralTool().customDialog(
                        context,
                        "Warning",
                        "Member name must be longer than 5 characters",
                        '',
                        DialogType.noHeader);
                    return;
                  }
                  if (memberBaruParentDropDownValue == null) {
                    GeneralTool().customDialog(
                        context,
                        "Warning",
                        "Select a parent for new member",
                        '',
                        DialogType.noHeader);
                    return;
                  }
                  context.read<CreateMemberBloc>().add(CreateMember(
                      memberBaruName, memberBaruParentDropDownValue!.sId!));
                },
                child: Container(
                  height: SizeConfig.screenHeight! * 0.05,
                  width: SizeConfig.screenWidth! * 0.08,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(10.0)),
                  child: const Center(
                      child: Text(
                    'Submit',
                    style: TextStyle(color: Colors.white),
                  )),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget migrasiMemberForm(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.screenHeight! * 0.02),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('Migrasi Member/Pindah parent',
              style: TextStyle(fontWeight: FontWeight.bold)),
          Row(
            children: [
              SizedBox(
                // height: SizeConfig.screenHeight! * 0.05,
                width: SizeConfig.screenWidth! * 0.2,
                child: DropdownButton<Member>(
                  isExpanded: true,
                  value: migrateMemberDropDownValue,
                  hint: const Text('Select Member'),
                  // icon: const Icon(Icons.arrow_downward),
                  elevation: 16,
                  style: const TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (Member? value) {
                    // This is called when the user selects an item.
                    setState(() {
                      migrateMemberDropDownValue = value;
                    });
                    getPossibleParent(context);
                  },
                  items: members.map<DropdownMenuItem<Member>>((Member value) {
                    return DropdownMenuItem<Member>(
                      value: value,
                      child: Text(value.name!),
                    );
                  }).toList(),
                ),
              ),
              SizedBox(
                width: SizeConfig.screenWidth! * 0.04,
              ),
              SizedBox(
                // height: SizeConfig.screenHeight! * 0.05,
                width: SizeConfig.screenWidth! * 0.2,
                child: DropdownButton<Member>(
                  isExpanded: true,
                  value: migratiMemberParentDropDownValue,
                  hint: const Text('Select Parent'),
                  // icon: const Icon(Icons.arrow_downward),
                  elevation: 16,
                  style: const TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (Member? value) {
                    // This is called when the user selects an item.
                    setState(() {
                      migratiMemberParentDropDownValue = value!;
                    });
                  },
                  items: migrasiMemberParents
                      .map<DropdownMenuItem<Member>>((Member value) {
                    return DropdownMenuItem<Member>(
                      value: value,
                      child: Text(value.name!),
                    );
                  }).toList(),
                ),
              ),
              SizedBox(
                width: SizeConfig.screenWidth! * 0.04,
              ),
              GestureDetector(
                onTap: () {
                  if (migrateMemberDropDownValue == null) {
                    GeneralTool().customDialog(
                        context,
                        "Warning",
                        "Select member to migrate",
                        '',
                        DialogType.noHeader);
                    return;
                  }
                  if (migratiMemberParentDropDownValue == null) {
                    GeneralTool().customDialog(
                        context,
                        "Warning",
                        "Select parent for migrating member",
                        '',
                        DialogType.noHeader);
                    return;
                  }
                  context.read<MigrateMemberBloc>().add(MigrateMember(migrateMemberDropDownValue!.sId!, migratiMemberParentDropDownValue!.sId!));
                },
                child: Container(
                  height: SizeConfig.screenHeight! * 0.05,
                  width: SizeConfig.screenWidth! * 0.08,
                  decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(10.0)),
                  child: const Center(
                      child: Text(
                    'Submit',
                    style: TextStyle(color: Colors.white),
                  )),
                ),
              ),
              // SizedBox(
              //   width: SizeConfig.screenWidth! * 0.02,
              // ),
              // Container(
              //   height: SizeConfig.screenHeight! * 0.05,
              //   width: SizeConfig.screenWidth! * 0.1,
              //   decoration: BoxDecoration(
              //       color: const Color.fromARGB(255, 142, 138, 138),
              //       borderRadius: BorderRadius.circular(10.0)),
              //   child: const Center(
              //       child: Text(
              //     'Result',
              //     style: TextStyle(color: Colors.white),
              //   )),
              // )
            ],
          ),
        ],
      ),
    );
  }

  Widget rectangleWidget(Member member) {
    return InkWell(
      onTap: () {
        print('clicked');
      },
      child: Container(
          padding: const EdgeInsets.all(16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            boxShadow: [
              BoxShadow(color: Colors.blue.shade100, spreadRadius: 1),
            ],
          ),
          child: Column(
            children: [
              Text(member.name!),
              member.name == 'Admin'
                  ? Container()
                  : Text(
                      'Level : ${member.level}',
                      style: const TextStyle(fontSize: 10),
                    ),
              member.name == 'Admin'
                  ? Container()
                  : Text(
                      'Bonus : \$${member.bonus}',
                      style: const TextStyle(fontSize: 10),
                    ),
            ],
          )),
    );
  }

  Widget treeGraph(BuildContext context) {
    return InteractiveViewer(
        constrained: false,
        boundaryMargin: const EdgeInsets.all(100),
        minScale: 0.01,
        maxScale: 0.9,
        child: Align(
          alignment: Alignment.center,
          child: GraphView(
            graph: graph,
            algorithm:
                BuchheimWalkerAlgorithm(builder, TreeEdgeRenderer(builder)),
            paint: Paint()
              ..color = Colors.green
              ..strokeWidth = 1
              ..style = PaintingStyle.stroke,
            builder: (Node node) {
              MemberNodes member =
                  nodes.firstWhere((element) => element.node == node);
              // I can decide what widget should be shown here based on the id
              return rectangleWidget(member.member);
            },
          ),
        ));
  }
}

class MemberNodes {
  Member member;
  Node node;
  MemberNodes(this.member, this.node);
}
