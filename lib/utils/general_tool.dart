import 'package:awesome_dialog/awesome_dialog.dart';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:getwidget/components/loader/gf_loader.dart';
import 'package:getwidget/types/gf_loader_type.dart';
import 'package:simple_mlm/size_config.dart';

abstract class GeneralToolBase {}

class GeneralTool extends GeneralToolBase {
  static final GeneralTool _instance = GeneralTool._internal();

  factory GeneralTool() {
    return _instance;
  }



  GeneralTool._internal();

  customDialog(BuildContext context, String tittle, String content, String link,
      DialogType type,
      {bool overrideLink = false}) {
    AwesomeDialog(
      // headerAnimationLoop: false,
      context: context,
      dismissOnTouchOutside: false,
      btnCancelColor: Colors.red,
      btnCancel: MaterialButton(
          color: Colors.red,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          child: const Text("Ok",
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
          onPressed: () {
            if (link == '') {
              Navigator.pop(context);
            } else {
              if (overrideLink) {
                // Navigator.pop(context);
                Navigator.pushNamedAndRemoveUntil(
                    context, link, (route) => false);
              } else {
                Navigator.pushNamed(context, link);
              }
            }
          }),
      dialogType: type,
      // animType: AnimType.TOPSLIDE,
      title: tittle,
      desc: content,
    ).show();
  }

  Widget loaderDialog(String message) {
    return Center(
      child: Container(
        height: SizeConfig.screenHeight! * 0.3,
        width: SizeConfig.screenWidth! * 0.6,
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Text(
              message,
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 16),
            ),
            const Spacer(),
            const GFLoader(
              type: GFLoaderType.circle,
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }

  // void flushbarSuccess(BuildContext context, String message) {
  //   Flushbar(
  //     title: "Success",
  //     message: message,
  //     flushbarPosition: FlushbarPosition.TOP,
  //     icon: const Icon(
  //       Icons.error,
  //       color: Colors.white,
  //     ),
  //     duration: const Duration(seconds: 1),
  //     margin: const EdgeInsets.fromLTRB(8, 20, 8, 8),
  //     backgroundColor: Colors.green,
  //     borderRadius: 15,
  //     boxShadows: const [
  //       BoxShadow(
  //         offset: Offset(0.1, 0.0),
  //         blurRadius: 1.0,
  //       ),
  //     ],
  //   ).show(context);
  // }

  // void flushbarFailed(BuildContext context, String message) {
  //   Flushbar(
  //     title: "Failed",
  //     message: message,
  //     flushbarPosition: FlushbarPosition.TOP,
  //     icon: const Icon(
  //       Icons.error,
  //       color: Colors.white,
  //     ),
  //     duration: const Duration(seconds: 1),
  //     margin: const EdgeInsets.fromLTRB(8, 20, 8, 8),
  //     backgroundColor: Colors.red.shade700,
  //     borderRadius: 15,
  //     boxShadows: const [
  //       BoxShadow(
  //         offset: Offset(0.1, 0.0),
  //         blurRadius: 1.0,
  //       ),
  //     ],
  //   ).show(context);
  // }
}
