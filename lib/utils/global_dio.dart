// import 'package:alice/alice.dart';
import 'package:simple_mlm/configs/config.dart';
import 'package:dio/dio.dart';

class DIOGlobal {
  static final DIOGlobal _instance = DIOGlobal._internal();

  static Dio dio = Dio(BaseOptions(
    baseUrl: APIConfig.BASE_URL,
  ));

  // void initAlice(Alice alice) {
  //   dio.interceptors.add(alice.getDioInterceptor());
  // }

  factory DIOGlobal() {
    return _instance;
  }

  DIOGlobal._internal();
}
