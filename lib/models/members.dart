class Member {
  String? sId;
  String? name;
  int? level;
  double? bonus;
  String? parent;

  Member({this.sId, this.name, this.level, this.bonus, this.parent});

  Member.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
    level = json['level'];
    bonus = json['bonus'];
    parent = json['parent'] ?? "admin";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    data['name'] = name;
    data['level'] = level;
    data['bonus'] = bonus;
    data['parent'] = parent;
    return data;
  }
}