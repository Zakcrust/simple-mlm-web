import 'package:simple_mlm/models/members.dart';

class MigrateParentResponse {
  String? message;
  List<Member>? data;

  MigrateParentResponse({this.message, this.data});

  MigrateParentResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    if (json['data'] != null) {
      data = <Member>[];
      json['data'].forEach((v) {
        data!.add(Member.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

