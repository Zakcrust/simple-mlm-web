import 'package:simple_mlm/models/members.dart';

class MemberCreateResponse {
  String? message;
  Member? data;

  MemberCreateResponse({this.message, this.data});

  MemberCreateResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    data = json['data'] != null ? Member.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}
